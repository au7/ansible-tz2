**TZ2 на Ansible: Переадресация входящих запросов на сервер от src:dport на внутренний адрес int_address:int_port**

**TZ2 on Ansible: Forwarding incoming requests to the server from src:dport on internal int_adress:int_port**

Аналог команды в iptables как пример:
/sbin/iptables -t nat -A PREROUTING -p UDP  -s 7.7.8.0/23 --dport 5050 -j DNAT --to  127.0.0.1:53

Отображение команды:
iptables -L -n -t nat
Chain PREROUTING (policy ACCEPT)
target     prot opt source               destination
DNAT       udp  --  7.7.8.0/23           0.0.0.0/0            udp dpt:5050 to:127.0.0.1:53

Запускать: ansible-playbook playbook.yaml
